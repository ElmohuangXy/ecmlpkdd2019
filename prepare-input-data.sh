#!/bin/bash

typeset SCRIPT_DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

#set -xv

error()
{
   echo "ERROR: $*"
   return 1
}
typeset -xf error

main()
{
    cd ${SCRIPT_DIR}/input || { typeset RC=$?; error "$SCRIPT_NAME: Cannot execute cd ${SCRIPT_DIR}/input"; return $RC; }
    rm -f *.csv || { typeset RC=$?; error "$SCRIPT_NAME: Cannot execute rm -f *.csv"; return $RC; }
    echo "Unzip data files ..."
    for f in *.zip; do
		unzip -o $f -d . || { typeset RC=$?; error "$SCRIPT_NAME: Cannot execute unzip -o $f -d ."; return $RC; }
    done
    echo "Trim csv spurious headers/footers ..."
    typeset TMPFILE=file.tmp
    for f in LoanStats*.csv; do
        tail -n +2 $f|head -n -2>${TMPFILE} || { typeset RC=$?; error "$SCRIPT_NAME: Cannot execute tail -n +2 $f|head -n -2>${TMPFILE}"; return $RC; }
        mv ${TMPFILE} $f || { typeset RC=$?; error "$SCRIPT_NAME: Cannot execute mv ${TMPFILE} $f"; return $RC; }
    done
    # Execute R script to manipulate csv data
    echo "Run R script to process csv files. This may take a few minutes ..."
    Rscript ${SCRIPT_DIR}/preprocess_lending_club_files.R || { typeset RC=$?; error "$SCRIPT_NAME: Cannot execute R ${SCRIPT_DIR}/preprocess_lending_club_files.R"; return $RC; }
    cd ..
    echo "Success: csv files generated"
}
typeset -xf main

main "$@"


#!/bin/bash

typeset SCRIPT_DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
#. ${SCRIPT_DIR}/setup

find ${SCRIPT_DIR} \( -name output -o -name __pycache__ \) -type d -exec rm -rf {} \; 2>/dev/null

#!/bin/bash

typeset SCRIPT_DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

export PYTHONUNBUFFERED=1

mkdir -p ${SCRIPT_DIR}/output
typeset RESULTS_LOG=${SCRIPT_DIR}/output/results.log

run_base_model() # startYear endYear models numFolds seedList
{
    typeset START_YEAR=$1
    typeset END_YEAR=$2
    typeset MODELS=$3
    typeset NFOLD=$4
    typeset SEED_LIST=$5
    typeset LOG=${SCRIPT_DIR}/output/base_model-${START_YEAR}-${END_YEAR}-${MODELS}-${NFOLD}-${SEED_LIST}.log
    echo "*** taskset -c ${CPU} python3 build_base_model.py ${OPTS} ${START_YEAR} ${END_YEAR} ${MODELS} ${NFOLD} ${SEED_LIST}" 2>&1 | tee ${LOG}
    taskset -c ${CPU} python3 build_base_model.py ${OPTS} ${START_YEAR} ${END_YEAR} ${MODELS} ${NFOLD} ${SEED_LIST} 2>&1 | tee -a ${LOG}

    echo "base_model ${START_YEAR} ${END_YEAR} ${MODELS} ${NFOLD} ${SEED_LIST} results:" 2>&1 | tee -a ${RESULTS_LOG}
    cat ${LOG} | egrep 'Total train\+test row count' 2>&1 | tee -a ${RESULTS_LOG}
    cat ${LOG} | egrep 'Average Gini of' 2>&1 | tee -a ${RESULTS_LOG}
}
typeset -xf run_base_model

run_transfer_model() # startYear endYear models numFolds seedList
{
    typeset START_YEAR=$1
    typeset END_YEAR=$2
    typeset MODELS=$3
    typeset NFOLD=$4
    typeset SEED_LIST=$5
    typeset LOG=${SCRIPT_DIR}/output/transfer_model-${START_YEAR}-${END_YEAR}-${MODELS}-${NFOLD}-${SEED_LIST}.log
    echo "*** taskset -c ${CPU} python3 transfer_model_experiment.py ${OPTS} ${START_YEAR} ${END_YEAR} ${MODELS} ${NFOLD} ${SEED_LIST}" 2>&1 | tee ${LOG}
    taskset -c ${CPU} python3 transfer_model_experiment.py ${OPTS} ${START_YEAR} ${END_YEAR} ${MODELS} ${NFOLD} ${SEED_LIST} 2>&1 | tee -a ${LOG}

    echo "transfer_model ${START_YEAR} ${END_YEAR} ${MODELS} ${NFOLD} ${SEED_LIST} results:" 2>&1 | tee -a ${RESULTS_LOG}
    cat ${LOG} | egrep 'Total train\+test row count' 2>&1 | tee -a ${RESULTS_LOG}
    cat ${LOG} | egrep 'Average Gini of' 2>&1 | tee -a ${RESULTS_LOG}
}
typeset -xf run_transfer_model

#rm -f ${RESULTS_LOG}

# Cpu to run on, defaults to 0 unless overridden
# Required to obtain consistent results with Tensorflow
export CPU=${CUSTOM_CPU:-0}
export PYTHONHASHSEED=0

#export NFOLD=3
export NFOLD=10
export SEED_LIST=7,8,9,10,11
#export SEED_LIST=7
export OPTS=
# Uncomment to run the Car Loan experiments
#export OPTS=-c

if [ "${OPTS}" = "-c" ]; then
    for i in 2018; do
        run_base_model 2007 $i u-target ${NFOLD} ${SEED_LIST}
        run_base_model 2007 $i u-src ${NFOLD} ${SEED_LIST}
        run_base_model 2007 $i v ${NFOLD} ${SEED_LIST}
        run_transfer_model 2007 $i w ${NFOLD} ${SEED_LIST}
        run_transfer_model 2007 $i wx ${NFOLD} ${SEED_LIST}
        run_transfer_model 2007 $i wxy ${NFOLD} ${SEED_LIST}
        run_transfer_model 2007 $i wxyz ${NFOLD} ${SEED_LIST}
    done
else
    for i in 2011 2014 2016 2018; do
        run_base_model 2007 $i u-target ${NFOLD} ${SEED_LIST}
        run_base_model 2007 $i u-src ${NFOLD} ${SEED_LIST}
        run_base_model 2007 $i v ${NFOLD} ${SEED_LIST}
        run_transfer_model 2007 $i w ${NFOLD} ${SEED_LIST}
        run_transfer_model 2007 $i wx ${NFOLD} ${SEED_LIST}
        run_transfer_model 2007 $i wxy ${NFOLD} ${SEED_LIST}
        run_transfer_model 2007 $i wxyz ${NFOLD} ${SEED_LIST}
    done
fi

# ecmlpkdd2019

Code accompanying the paper 'Transfer Learning in Credit Risk' by H. Suryanto et. al., submitted to the 2019 European Conference on Machine Learning and Principles and Practice of Knowledge Discovery in Databases.

See readme-ecmlpkdd2019.pdf for details on how to run the experiments.

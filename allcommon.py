# Copyright 2019 Rich Data Corporation Pte Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

'''
Common functions to support Transfer Learning experiment
'''

import os
import pandas as pd
import numpy as np
import category_encoders as ce

from sklearn.utils import class_weight
from sklearn.model_selection import StratifiedKFold

from keras.models import Model, load_model
from keras.layers import Dense, Input
from keras.utils import plot_model

CC_DEBT_CONSOL = 'credcard_debtconsol'
SMALL_BUS = 'small_business'

DATAFILE_PATCH = True
# DATAFILE_PATCH = False

DIAGNOSTIC = True


class YearRange(object):
    '''
    Simple YearRange holder
    '''

    def __init__(self, startYear, endYear):
        self.startYear = startYear
        self.endYear = endYear

    def getRange(self):
        return range(self.startYear, self.endYear + 1)

    def __str__(self):
        return str(self.startYear) + '-' + str(self.endYear)


def readLendingClubDataFrame(inputDir, yearRange, finType, useCar):
    '''
    Read all required Lending Club files to build a DataFrame for
    the required year range
    '''
    if useCar:
        if finType == SMALL_BUS:
            filename = 'car_2007_2018.csv'
            return pd.read_csv(inputDir + filename)
        elif finType == CC_DEBT_CONSOL:
            filename = 'creditcard_2007_2018.csv'
            return pd.read_csv(inputDir + filename)
        else:
            raise Exception('Unknown finType=' + str(finType))

    startYear = yearRange.startYear
    endYear = yearRange.endYear
    filename = 'lending_club_' + str(startYear) + '_' + str(endYear) + '_' + str(finType) + '.csv'
    if startYear == 2007 and endYear in {2011, 2014, 2016, 2018}:
        return pd.read_csv(inputDir + filename)
    raise Exception('Unknown file: ' + filename)


def concatDfRows(df1, df2):
    '''
    return the unsorted concatenation of df1 + df2
    '''
    if df1 is None:
        return df2
    return pd.concat((df1, df2), sort = False)


def calcStats(df):
    '''
    Calculate statistics of dataframe df, such as mean, std, var, min, max, diff
    return a dataframe containing statistics
    '''
    # colAttribDf (column attributes Df), i.e. mean, std, etc of all cols
    colAttribDf = pd.DataFrame()
    colAttribDf["mean"] = df.mean()
    colAttribDf["std"] = df.std()
    colAttribDf["var"] = df.var()
    colAttribDf["min"] = df.min()
    colAttribDf["max"] = df.max()
    colAttribDf["diff"] = df.max() - df.min()
    colAttribDf["norm_range"] = 0

    # Calc norm_range for all cols (including outcome)
    for feature_name in df.columns:
        max_value = colAttribDf.loc[feature_name, 'max']
        min_value = colAttribDf.loc[feature_name, 'min']
        norm_range = max_value - min_value
        if norm_range == 0:
            norm_range = 1
        colAttribDf.loc[[feature_name], ["norm_range"]] = norm_range
    return colAttribDf


def read_data_generic(
        inputDir,  # input folder
        outputDir,  # output folder
        filename,  # filename
        trim_stdev,  # trim using mean plus minus (this number x stdev), for outlier removal
        opt_trim_features,  # numeric features to be trimmed
        onehot_features,  # features to be converted using one hot
        ordinal_features,  # ordinal features, should not be trimmed
        output_columns,  # output column, usually only one column
        sample_nrow_max,  # number of maximum sample, if there is not enough data, we will use all
        optDf = None
        ):
    '''
    Read data from a csv file,
    returning a numpy nd array and a column list
    '''
    if optDf is None:
        df = pd.read_csv(inputDir + filename)
    else:
        df = optDf.copy()

    if DIAGNOSTIC:
        print("df before resample shape =" + str(df.shape) + " type=" + str(type(df)))
    df = resample_without_replacement_df(df, sample_nrow_max)
    print('Total train+test row count=' + str(df.shape[0]))

    # convert categorical to numeric
    ce_one_hot = ce.OneHotEncoder(cols = onehot_features)
    df = ce_one_hot.fit_transform(df)
    ce_ordinal = ce.OrdinalEncoder(cols = ordinal_features)
    df = ce_ordinal.fit_transform(df)
    # ordinals are 1 offset, we want 0 -> 1 for boolean & 0
    # offset if there were any others, so subtract 1
    df.loc[:, ordinal_features] -= 1

    # NOTE: assumes ordinal_features are only for output
    allDerivedCsvCols = df.columns

    # convert to numerical
    df = df.apply(pd.to_numeric)

    colAttribDf = calcStats(df)

    if DIAGNOSTIC:
        print('before=' + str(df.shape))

    if opt_trim_features is None:
        # Trim based on all input cols
        trim_features = list(set(df.columns.difference(set(output_columns))))
    else:
        # Only trim based on selected input cols
        trim_features = opt_trim_features

    for feature_name in trim_features:
        mean_value = colAttribDf.loc[ feature_name, 'mean']
        std_value = colAttribDf.loc[ feature_name, 'std']
        df = df.loc[np.abs(df[feature_name] - mean_value) <= (trim_stdev * std_value)]
        print(feature_name + '=' + str(df.shape))

    if DIAGNOSTIC:
        df.to_csv(outputDir + filename + '_trimmed_not_normalised.csv', sep = ',')
    result = df.copy()
    for feature_name in df.columns:
        max_value = df[feature_name].max()
        min_value = df[feature_name].min()
        norm_range = max_value - min_value
        if norm_range == 0:
            norm_range = 1
        result[feature_name] = (df[feature_name] - min_value) / norm_range
    df = result
    if DIAGNOSTIC:
        df.to_csv(outputDir + filename + '_dataset_trimmed_normalised.csv', sep = ',')
        print('after=' + str(df.shape))
    dataset = df.values
    return dataset, allDerivedCsvCols


def calc_gini(fscore, foutcome):
    '''
    Calculate Gini based on a list of scores and a list of actual outcomes
    The scores are not calibrated to probability of defaults
    return the Gini value
    '''
    n = len(fscore)
    n_pos = sum(foutcome)
    # print("n_pos="+str(n_pos))
    df = pd.DataFrame({'score': fscore, 'outcome': foutcome})
    df = df.sort_values('score', ascending = False)
    df['seq'] = list(range(1, n + 1))
    df['cum_outcome'] = df['outcome'].cumsum()
    df['above'] = df['seq'] - df['cum_outcome']
    df['area'] = df['above'] * df['outcome']
    auc = 1 - (df['area'].sum() / (n_pos * (n - n_pos)))
    gini = auc * 2 - 1
    return gini


def resample_without_replacement_np(narr, sample_size):
    '''
    Resample without replacement
    input numpy array and sample size
    return numpy array
    '''
    if(narr.size < sample_size):
        print("narr.size=" + str(narr.size) + "  sample_size=" + str(sample_size))
        print("clamping because sample_size > narr.size")
        # clamp at max avail
        sample_size = narr.size
    indices = np.random.permutation(narr.shape[0])
    sample_idx, unused_idx = indices[:sample_size + 1], indices[sample_size + 1:]
    data_sample, data_unused = narr[sample_idx, :], narr[unused_idx, :]
    return data_sample


def resample_without_replacement_df(df, sample_size):
    '''
    Resample without replacement
    input dataframe and sample size
    return dataframe
    '''
    np_arr = df.values
    cols = df.columns.tolist()
    if(np_arr.size < sample_size):
        print("sample_size > np_arr.size")
        # clamp at max avail
        sample_size = np_arr.size
    indices = np.random.permutation(np_arr.shape[0])
    sample_idx, unused_idx = indices[:sample_size + 1], indices[sample_size + 1:]
    data_sample, data_unused = np_arr[sample_idx, :], np_arr[unused_idx, :]
    data_sample_df = pd.DataFrame(data_sample, columns = cols)
    return data_sample_df


def random_split(df, split_index):
    '''
    Resample a dataframe into two dataframes based on split index
    return two dataframes
    '''
    if(df.size < split_index):
        print("split_index > df.size")
        exit(1)
    indices = np.random.permutation(df.shape[0])
    first_idx, second_idx = indices[:split_index + 1], indices[split_index + 1:]
    first_sample, second_sample = df[first_idx, :], df[second_idx, :]
    return first_sample, second_sample


def weight_balance(Yvar):
    '''
    Configure class weight
    input a list of outputs
    return class weight
    '''
    return class_weight.compute_class_weight('balanced', np.unique(Yvar), Yvar)


def xy_split_cols(df, number_of_cols):
    '''
    Split column wise, the last column is always Y, while the remainders are X
    input dataframe and the number of columns
    returning dataframe for all inputs and the output
    '''
    Xvar = df[:, 0:number_of_cols - 1]
    Yvar = df[:, number_of_cols - 1]
    return Xvar, Yvar


def model_generic(
        number_of_inputs,
        number_of_outputs,
        number_of_layers,
        number_of_nodes,
        # model_name
        ):
    '''
    Configure a generic, simple linear model
    inputs: number of inputs, outputs, layers and nodes
    return a model
    '''

    number_of_all_layers = number_of_layers + 2
    dl_layer = [None] * (number_of_all_layers)
    dl_layer[0] = Input(shape = (number_of_inputs,))  # the input layer

    # iterate from 1 to number of layer, for loop with range is (inclusive, exclusive), so +1 is required
    for i in range(1, number_of_layers + 1):
        dl_layer[i] = Dense(number_of_nodes, activation = 'relu')(dl_layer[i - 1])

    output = Dense(number_of_outputs, activation = 'sigmoid')(dl_layer[number_of_layers])
    one_model = Model(inputs = [dl_layer[0]], outputs = output)
    one_model.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
    return one_model


def build_model_generic(
        cv_fold,  # string: number of cross validation folds
        n_epochs,  # number of epochs, 1 epoch = 1 pass through the whole training data
        n_row,  # number of rows used as training+test from the whole dataset
        n_fold,  # int: number of folds, e.g. 10 folds
        n_batch,  # number of rows for each batch process
        trim_stdev,  # trim any outliers
        verbose_level,  # verbose level, e.g. 0
        description,  # description of the model
        filename,  # filename of the data
        n_nodes,  # number of nodes in each layer, in this setup, the number of nodes for each layer is the same
        predef_model,  # the model, pass by function name
        n_cols,  # total number of columns
        n_layers,  # total number of layers
        input_model_name,  # model name
        output_model_name,  # model name
        list_of_seeds,  # list of random seeds
        inputDir,
        outputDir,
        output_columns,
        onehot_features,
        ordinal_features,
        opt_trim_features,

        is_using_basemodel,
        input_basemodel_fullpath,
        optDf = None
        ):
    '''
    Build, train, and test a generic model
    input: see below
    return the average model and the performance of each model created
    '''

    number_of_models = len(list_of_seeds) * n_fold
    performance = pd.DataFrame(index = np.arange(0, number_of_models), columns = ('repeat', 'description', 'iteration', 'training', 'test', 'gini'))

    number_of_outputs = 1
    number_of_inputs = n_cols - number_of_outputs  # all columns - 1 output = number of inputs
    repeat = 0

    # for all random seeds, repeat
    for a_random_seed in list_of_seeds:
        np.random.seed(a_random_seed)
        print('input_model_name=' + input_model_name + " random seed=" + str(a_random_seed))
        print('output_model_name=' + output_model_name + " random seed=" + str(a_random_seed))
        sample_nrow_max = n_row

        # read data
        full_data, columns_list = read_data_generic(
                            inputDir,
                            outputDir,
                            filename,
                            trim_stdev,
                            opt_trim_features,
                            onehot_features,
                            ordinal_features,
                            output_columns,
                            sample_nrow_max,
                            optDf)

        print("Full Data shape =" + str(full_data.shape) + " type=" + str(type(full_data)))
        subset_data = resample_without_replacement_np(full_data, n_row)
        nrow_actual = subset_data.shape[0]
        print('Total train+test row count=' + str(nrow_actual))
        np.savetxt(outputDir + filename + '_regular_extract.csv', subset_data, delimiter = ',')

        # split to prepare n folds cross validation
        Xfull, Yfull = xy_split_cols(subset_data, n_cols)
        skf = StratifiedKFold(n_splits = n_fold, random_state = None, shuffle = False)
        print("Number of splits = " + str(skf.get_n_splits(Xfull, Yfull)))
        class_weights = weight_balance(Yfull)

        if is_using_basemodel:
            a_model = load_model(input_basemodel_fullpath)
        else:
            a_model = predef_model(
                number_of_inputs,
                number_of_outputs,
                n_layers,
                n_nodes
                )

        if DIAGNOSTIC and is_using_basemodel:
            print("start base model summary")
            print(a_model.summary())
            print("end base model summary")

        a_model.save(outputDir + output_model_name + "_TEMPLATE" + ".h5")

        iteration = 0

        # for all splits
        for train_index, test_index in skf.split(Xfull, Yfull):
            Xtrain, Xtest = Xfull[train_index], Xfull[test_index]
            Ytrain, Ytest = Yfull[train_index], Yfull[test_index]
            a_model = load_model(outputDir + output_model_name + "_TEMPLATE" + ".h5")
            if is_using_basemodel == False:
                a_model.fit(Xtrain, Ytrain, epochs = n_epochs, batch_size = n_batch, class_weight = class_weights, verbose = verbose_level)
            else:
                a_model = load_model(outputDir + output_model_name + "_TEMPLATE" + ".h5")
            Ypred = (a_model.predict(Xtest)).flatten()
            model_index = (repeat * n_fold) + iteration
            a_model.save(outputDir + output_model_name + "_" + cv_fold + "_" + str(model_index) + ".h5")
            performance.loc[model_index] = repeat, description, iteration, Ytrain.size, Ytest.size, calc_gini(Ypred, Ytest)
            print("Train:", train_index.size, "   Test:", test_index.size, "   Gini:", calc_gini(Ypred, Ytest))
            iteration = iteration + 1
        repeat = repeat + 1
    avg_gini = performance.loc[:, "gini"].mean()

    error = [None] * (number_of_models)
    min_error = 999
    min_model_index = -1
    for i in range(0, number_of_models):
        error[i] = abs(performance.loc[i, "gini"] - avg_gini)
        if error[i] < min_error:
            min_error = error[i]
            min_model_index = i
    output_model_filename = outputDir + output_model_name + "_" + cv_fold + "_" + str(min_model_index) + ".h5"
    print("Loading the average model=" + output_model_filename)
    average_model = load_model(output_model_filename)
    return average_model, performance

